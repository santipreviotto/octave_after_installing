# Things to Do After Installing Fedora 34

## Install dependencies

```
liboctave-dev
```
```
python3-pip 
```
```
pip3 install sympy==1.5.1
```
```
pip3 install mpmath
```

## Import into python

```
import sympy
```
```
import mpmath
```

## Install additional packages 

```
octave-communications 
```
```
octave-signal
```
```
octave-symbolic
```

Then in each octave file include the packages to use.

```
pkg load namepackage
```